# Assembly Checklist
1. Place the landing gear down facing forward
2. Remove the Alta X fuselage from its carrying case
3. Slowly lower the Alta X fuselage down onto the landing gear
    1. You can align the fuselage and landing gear by looking through the center of the fuselage
4. Rotate the fuselage so that the index marks line up
5. Secure the frog latch
6. Undo the propeller tie-downs and secure tie-downs
7. Rotate the arms clockwise and latch into their fully extended position
8. Place the batteries into the battery trays with the cables facing towards the center of the drone nearest the battery ports.  Align the batteries to opposing corners
9. Connect the battery tie downs using the second from last hole

# Pre-Flight Inspection
1. Controller
    1. RTL Switch to STANDBY
    2. Mode Switch to POSITION
    3. MISSION switch to STANDBY
    4. GIMBAL switch to OFF
    5. All sticks centered
    6. 900 MHz antennas oriented at 45 and -45 degrees
    7. Controller battery > 7.6 V
2. Aircraft
    1. All propellers are intact, no dings, cracks, or warping
    2. All propeller blades extend smoothly
    3. All propeller vibration dampeners are intact, no tearing
    4. All propellers spin freely, no clicking or wobbling
    5. All motor mounts are secure, no loose screws or wobbling
    6. All motor cables are intact, no exposed copper or kinks
    7. All booms are intact, no dings, cracks, warping, or twisting
    8. All boom struts are intact, no dings, cracks or excessive warping
    9. Both boom extension ring clamps are locked
    10. All frame panels are installed
    11. Both 900 MHz antennas are fully screwed on
    12. Both GPS antennas are secured
    13. All 4 battery straps are intact, no cracks or stretch marks
    14. All 4 battery straps are properly installed on the second from the last hole
    15. Both batteries are firmly seated against opposing corners
    16. Both battery leads are facing the center rear of the aircraft
    17. Carry handle is appropriately secured
    18. Aircraft front is aligned with the landing gear
    19. Payload power cable is appropriately secured
    20. Payload GPS cable is appropriately secured
    21. All 18 baseplate vibration dampeners (isolator cartridges) are intact and secure, no cracks
    22. Landing gear clamp is fully closed
    23. Payload clamp is fully closed
    24. Any payload present is properly installed and configured
    25. Landing gear is rigid and intact, no dings, cracks, or warping.

# Hyperspectral Camera
## Installation Procedure
1. Unlatch the payload securing latch
2. While holding down the payload release toggle, insert the hyperspectral camera into the payload dock
3. Release the payload release toggle
4. Rotate the hyperspectral camera to align
5. Extend and secure the Payload GPS antenna
6. Connect the Payload GPS antenna feedline to the camera
7. Secure the Payload GPS antenna to the velcro strap on the landing gear
8. Connect the Payload Power Adapter cable to the aircraft and the camera
9. Secure the Payload Power Adapter cable to the velcro strap on the landing gear

# Packing Checklist
1. Disconnect and remove the batteries
2. Disconnect and remove any installed payloads
3. Lower payload GPS antenna, leave floating
4. Unlatch the boom extension ring clamps
5. Rotate the arms counterclockwise until flush against the side of the fuselage
6. Attach the propeller tie downs to the propellers.  When facing the Headwall sticker, connect the left propellers to each other and the right propellers to each other
7. Reattach the foam and plastic protectors to the controller
8. Replace the controller in the case
9. Store the 900 MHz antennas in the case
10. Unlatch the Alta X fuselage from the landing gear
11. Lower the Alta X fuselage into the case with the Headwall sticker nearest the opening
12. Gently push the Alta X fuselage all the way into the case.  All the motors should be below the top of the case

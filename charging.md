# Main Battery Charging
1. Locate the following items:
    1. Flight Battery Set (2x Tatu Plus)
    2. U4-Pro Charger
2. Plug in the U4-Pro Charger into the wall.
3. Turn on the U4-Pro Charger using the switch on the back.
4. Plug in each battery set to the main charge port and the balance port.
5. Wait 10 seconds for the U4-Pro charger to automatically start charging.
6. Monitor the batteries until the CC phase is complete.
